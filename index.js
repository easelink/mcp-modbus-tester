'use strict'

const modbus = require('jsmodbus')
const net = require('net')
const { start } = require('repl')
const socket = new net.Socket()
const options = {
  host: '10.43.10.24',
  // host: '127.0.0.1',
  port: '502'
}
let client = new modbus.client.TCP(socket, 61, 100)

function toBinary(x, width) {
  return x.toString(2).padStart(width, 0)
}

function binaryConverter(width) {
  return x => toBinary(x, width)
}

function toHex(x, width) {
  return x.toString(16).padStart(width, 0)
}

function hexConverter(width) {
  return x => toHex(x, width)
}

function applyTransformFn(fn, value) {
  if (fn === undefined) return value;
  return fn(value)
}

async function readRegisters(startAddress, n, transformFn) {
  const r = await client.readHoldingRegisters(startAddress, n)
  const values = r.response._body.valuesAsArray
  return applyTransformFn(transformFn, values)
}

function print(address, name, value) {
  console.log(`${address.toString(10).padStart(4, ' ')}  ${name}: ${value}`)
}

async function read16(address, name, transformFn) {
  const value = await readRegisters(address, 1, xs => applyTransformFn(transformFn, xs[0]))
  print(address, name, value)
  return value
}

async function read16s(startAddress, names, transformFn) {
  const values = await readRegisters(startAddress, names.length, xs => xs.map(x => applyTransformFn(transformFn, x)))
  for (let i = 0; i < names.length; i++) {
    if (names[i] != 0) {
      print(startAddress + i, names[i], values[i])
    }
  }
  return values
}

async function readString(startAddress, name, length) {
  const value = await readRegisters(startAddress, length, xs => xs.map(x => String.fromCharCode((x >> 8) & 0xFF, x & 0xFF)).join(''))
  print(startAddress, name, value)
  return value
}

async function read32(startAddress, name, transformFn) {
  const value = await readRegisters(startAddress, 2, xs => applyTransformFn(transformFn, (xs[0] << 0) | (xs[1] << 16)))
  print(startAddress, name, value)
  return value
}

async function read64(startAddress, name, transformFn) {
  function toUint64(bytes) {
    const xs = bytes.map(x => BigInt(x))
    return (xs[0] << BigInt(0)) | (xs[1] << BigInt(16)) | (xs[2] << BigInt(32)) | (xs[3] << BigInt(48))
  }

  const value = await readRegisters(startAddress, 4, xs => applyTransformFn(transformFn, toUint64(xs)))
  print(startAddress, name, value)
  return value
}

function printHeader(title) {
  console.log(`\n${title}\n----`)
}

async function test_secc() {
  // 100+ ---------------------------------------------
  printHeader(' 100+ Betrieb Ladepunkt LP')
  await read16(101, 'Matrix Charging Aktueller Fehler - SECC')
  await read16(102, 'Matrix Charging Aktueller Fehler - Quittierung')

  // 300+ ---------------------------------------------
  printHeader(' 300+ Betrieb Allgemein')
  await read16(303, 'Temperatur', x => `${x/100}°C`)
  await read16(306, 'Controller-Status')

  // 500+ ---------------------------------------------
  printHeader(' 500+ Betrieb Allgemein')
  await read16(500, 'Anzahl Ladepads')

  // 700+ ---------------------------------------------
  printHeader(' 700+ Herstellerangaben')
  await readString(700, 'Herstellerbezeichnung', 16)
  await readString(716, 'Gerätebezeichnung', 16)
  await readString(732, 'Firmwareversion', 16)
  await readString(748, 'Seriennummer', 16)
  await read16(764, 'Protokollrevision Modbus-Registersatz')

  // 1000+ --------------------------------------------
  printHeader('1000+ Reset')
  await read16s(1000, [
    'Reset MCP',
    'Reset MCP',
    0,
    'Reset to Default'
  ])
  await read16(1099, 'reserviert')
}

async function test_mcp() {
  // 100+ ---------------------------------------------
  printHeader(' 100+ Betrieb Ladepunkt LP')
  await read16(100, 'Ladefreigabe')
  await read16(101, 'Ladestromvorgabe', x => `${x}A`)
  await read16(120, 'Ladefreigabe unwirksam')
  await read16(121, '-', binaryConverter(11))
  await read16s(122, [
    'Zählerstand Low Word',
    'Zählerstand High Word',
    'Ladeleistung Low',
    'Ladeleistung High',
    'Aktueller Mode3-Ladestromwert',
    'Maximaler Mode3-Ladestromwert',
    'Stromtragfähigkeit Ladekabel'])

  await readString(129, 'RFID-Tag', 10)
  await readString(139, 'CP-Signal Zustand', 1)

  await read16s(141, [
    'PP Status',
    0, 0, 0,
    'Ladepunktstatus',
    'Kommunikationsfehler zum Fahrzeug',
    'Konfigurationsfehler am LP',
    0, 0,
    '-' ])

  await read32(151, 'Ladedauer', x => `${x} (${x / 1000 / 60}min)`)
  await read32(153, 'Kontaktierungsdauer', x => `${x} (${x / 1000 / 60}min)`)

  await read64(160, 'Zählerstand', x => `${x} (${x / BigInt(10**6)}MWh)`)

  await read16s(164, [
    'Strom I1', 'Strom I2', 'Strom I3',
    'Spannung U1', 'Spannung U2', 'Spannung U3',
    'Wirkleistung P1', 'Wirkleistung P2', 'Wirkleistung P3',
    'Blindleistung Q1', 'Blindleistung Q2', 'Blindleistung Q3',
    'Scheinleistung S1', 'Scheinleistung S2', 'Scheinleistung S3',
    'Max. Wirkleistung',
    'Primärphase',
    'Fail-Safe Status' ]);

  await read64(182, 'Zählerstand Session', x => `${x} (${x / BigInt(10**3)}kWh)`)

  await read16s(186, [
    'Positionierungsindikation',
    'Connector-Status',
    'Matrix Charging Aktueller Fehler',
    'Infrastruktur-Status'
  ])

  // 300+ ---------------------------------------------
  printHeader(' 300+ Betrieb Allgemein')

  await read16s(300, [
    'Betriebsmodus des ChargeControllers',
    'Versorgungsspannung',
    'Netzspannung'
  ])
  await read16(303, 'Temperatur', x => `${x/100}°C`)
  await read16(306, 'Controller-Status')
  await read16(307, 'MCP Operation Mode')

  // 500+ ---------------------------------------------
  printHeader(' 500+ Konfiguration LP')
  await read16s(500, [
    0,
    'Default-Ladestrom',
    0, 0, 0, 0, 0,
    'Fail-Safe-Ladestart',
    'Fail-Safe-Ampere',
    'Fail-Safe-Zeit'
  ])

  // 700+ ---------------------------------------------
  printHeader(' 700+ Herstellerangaben')
  await readString(700, 'Herstellerbezeichnung', 16)
  await readString(716, 'Gerätebezeichnung', 16)
  await readString(732, 'Firmwareversion', 16)
  await readString(748, 'Seriennummer', 16)
  await read16(764, 'Protokollrevision Modbus-Registersatz')

  // 1000+ --------------------------------------------
  printHeader('1000+ Reset')
  await read16s(1000, [
    'Reset MCP',
    'Reset MCP',
    0,
    'Reset to Default'
  ])
  await read16(1099, 'reserviert')

  // 1100+ --------------------------------------------
  printHeader('1100+ Erweiterte Fahrzeug-Werte LP')
  await read16(1100, 'Erweiterte Fahrzeugkommunikation')
  await readString(1101, 'Fahrzeug-ID', 4)
  await read16s(1105, [
    'Phasenmodus des Fahrzeuges',
    'Batterieladezustand'
  ])
  await read16s(1107, [
    'minimal möglicher Ladestrom des Fahrzeuges',
    'maximal möglicher Ladestrom des Fahrzeuges'
  ], x => `${x/100}A`)
  await read16(1109, 'Batteriekapazität (netto)', x => `${x/100}kWh`)
}

socket.on('connect', async function () {
  try {
    await test_mcp()
    // await test_secc()

    // Exceptions ---------------------------------------
    // try { 
    //   await read16(1200, 'Invalid address') 
    // } catch (e) { 
    //   console.log(e) 
    // }
    // 
    // try { 
    //   await read16(1200, 'Invalid address') 
    // } catch (e) { 
    //   console.log(e) 
    // }

    // Write example ------------------------------------
    // resp = await client.writeMultipleRegisters(101, [0x0020])
    // console.log(resp)

  } catch (e) {
    console.error(require('util').inspect(e, {
      depth: null
    }))
  } finally {
    console.log()
    socket.end()
  }

})

socket.on('error', console.error)

function logSocketEvent(eventName) {
  socket.on(eventName, e => {
    console.log(`Socket event '${eventName}' occured`)
  })
}
['end', 'timeout'].forEach(logSocketEvent)

socket.on('close', hadError => console.log(`Socket has been closed (hadError = ${hadError})`))

socket.connect(options)
